import express from 'express';
import helpers from '../helpers/res';
const Router = express.Router();


Router.route('/insertpiano')
    .post(helpers.addPiano)
  
Router.route('/insertsalesman')
   .post(helpers.addsalesman)

Router.route('/insertsale')
    .post(helpers.addSale)

Router.route('/insertrental')
    .post(helpers.addRental)

Router.route('/updaterental')
    .put(helpers.putRental)

Router.route('/insertconsignment')
    .post(helpers.addConsign)


Router.route('/updateconsignment')
    .put(helpers.updateConsign)

Router.route('/salesresult')
    .get(helpers.getsalesresult)



module.exports = Router;
