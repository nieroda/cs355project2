import db from '../dal/dbinsert';
import dbquery from '../dal/dbquery';


exports.addPiano = (req, res) => {
    db.insertUniversal(req.body, 'Piano')
    .then(res.json('{"success": true}'))
    .catch(err => {
      res.json(`{"error": ${err}}`);
    });
}

exports.addsalesman = (req, res) => {
    db.insertUniversal(req.body, 'Salesman')
    .then(res.json('{"success": true}'))
    .catch(err => {res.json(`{"error": ${err}}`)});
}

exports.addSale = (req, res) => {
    db.insertUniversal(req.body, 'Sale')
    .then(res.json('{"success": true}'))
    .catch(err => { res.json(`{"error": ${err}}`)});
}

exports.addRental = (req, res) => {
    db.insertUniversal(req.body, 'Rental')
    .then(res.json('{"success": true}'))
    .catch(err => {res.json(`{"error": ${err}}`)});
}

exports.addConsign = (req, res) => {
    db.insertUniversal(req.body, 'Consignment')
    .then(res.json('{"success": true}'))
    .catch(err => res.json(`{"error": ${err}}`));
}

exports.updateConsign = (req, res) => {
    dbquery.soldConsignment(req.body)
    .then(result => {
      result.changedRows === 0 ? res.json('{"success": false}') : res.json('{"success": true}');
    })
    .catch(err => {res.json(`{"error": ${err}}`)});
}


exports.putRental = (req, res) => {
    dbquery.endRental(req.body)
    .then(result => {
      result.changedRows === 0 ? res.json('{"success": false}') : res.json('{"success": true}');
    })
    .catch(err => {res.json(`{"error": ${err}}`)});
}

exports.getsalesresult = (req, res) => {
    Promise.all([dbquery.getSales(), dbquery.getRentals(), dbquery.getSale(), dbquery.getConsign()])
    .then(result => {
        res.json(result);
    })
    .catch(err => {
        res.json(`{"error": ${err}}`);
    });
}
