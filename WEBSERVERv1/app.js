import express from 'express';
let app = express();
import bodyParser from 'body-parser';
import myRoutes from './routes/router';
import methodOverride from 'method-override';
import cors from 'cors';

const port = process.env.PORT || 3000;
const ip = process.env.IP || '192.168.1.1';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(methodOverride('_method'));
app.use(cors());

app.use('/', myRoutes);


app.listen(port, () => {
    console.log(`App is running on port ${port}`);
})
