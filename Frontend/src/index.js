import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import promise from 'redux-promise';

import App from './components/app';
import reducers from './reducers';

import MyHeaders from './components/header';
import Footer from './components/footer';
import PostList from './components/post_list';
import PianoScreen from './components/piano_screen';

import { InsertPiano, InsertSalesman, InsertRental, InsertConsignment, InsertSale } from './components/inserts';
import { UpdateConsignment, UpdateRental } from './components/updates';



const createStoreWithMiddleware = applyMiddleware(promise)(createStore);

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <BrowserRouter>
        <div>
            <MyHeaders />
            <Switch>
              <Route path='/salesresult' component={PostList} />
              <Route path='/insertpiano' component={InsertPiano} />
              <Route path='/insertsalesman' component={InsertSalesman} />
              <Route path='/insertconsignment' component={InsertConsignment} />
              <Route path='/insertrental' component={InsertRental} />
              <Route path='/insertsale' component={InsertSale} />
              <Route path='/updaterental' component={UpdateRental} />
              <Route path='/updateconsignment' component={UpdateConsignment} />
              <Route path='/' component={PianoScreen} />
            </Switch>
            <Footer />
        </div>
    </BrowserRouter>
  </Provider>
  , document.querySelector('.container'));
