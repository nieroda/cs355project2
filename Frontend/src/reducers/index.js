import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import salesResult from './reducer_sales';

const rootReducer = combineReducers({
  salesResult,
  form: formReducer
});

export default rootReducer;
