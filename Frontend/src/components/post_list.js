import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../actions';
import { Link } from 'react-router-dom';

import { CON_TABLE, SaleTable, STble, RentalTable } from './tables';

class PostList extends Component {

  componentDidMount() {
    this.props.fetchPosts();
  }

  renderPosts() {
    if (this.props.res.data) {
      let [ Sales, Rental, Sale, Cons ] = this.props.res.data;
      return this.makeTable(Cons, Sale, Sales, Rental);
    }
  }

  makeTable(Cons, Sale, Sales, Rental) {
    return (
      <div>
        <STble data={Sales} />
        <RentalTable data={Rental} />
        <SaleTable data={Sale} />
        <CON_TABLE data={Cons} />
      </div>
    );
  }

  render() {
    return (
      <div className='container'>
        {this.renderPosts()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    res: state.salesResult
  }
}

export default connect(mapStateToProps, {fetchPosts})(PostList);
