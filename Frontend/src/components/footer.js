import React, { Component } from 'react';

class Footer extends Component {

  render() {
    return (
      <footer className="footer text-center">
      <div className="container center">
         <p>Check out my <a href="https://github.com/nieroda/PianoReactExpress"> code!  </a></p>
      </div>
    </footer>
    )
  }
}

export default Footer;
