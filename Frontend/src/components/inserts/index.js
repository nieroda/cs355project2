export InsertConsignment from './insert_consignment';
export InsertPiano from './insert_piano';
export InsertRental from './insert_rental';
export InsertSale from './insert_sale';
export InsertSalesman from './insert_salesman';
