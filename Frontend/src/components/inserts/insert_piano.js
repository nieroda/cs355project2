import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createPiano } from '../../actions/index';

class InsertPiano extends Component {

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'had-danger' : ""}`;
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className='form-control'
          type='text'
          {...field.input}
        />
        <div className='text-help'>
          {touched ? error : ''}
        </div>
      </div>
    )
  }

  onSubmit(values) {
    this.props.createPiano(values, () => {
      this.props.history.push('/salesresult');
    });
  }

  render() {
    const { handleSubmit } = this.props;


    return (
      <div className='container'>
        <div className='jumbotron'>
          <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
            <Field
                label='Serial Number'
                name='serial_number'
                component={this.renderField}
                />
            <Field
                label='Brand'
                name='brand'
                component={this.renderField}
              />
            <Field
                label='Model'
                name='model'
                component={this.renderField}
                />
            <Field
                label='Dealer Price'
                name='dealer_price'
                component={this.renderField}
                />
            <button type='submit' className='btn btn-primary'>Submit</button>
            <Link to='/' className='btn btn-danger'>Cancel</Link>
          </form>
        </div>
      </div>
    );
  }
}

function validate(values) {
  var errs = {};

  if (!values.serial_number) {
    errs.serial_number = "You must enter a serial number";
  }
  if (!values.brand) {
    errs.brand = "You must enter a brand";
  }
  if (!values.model) {
    errs.model = "You must enter a model";
  }
  if (!values.dealer_price) {
    errs.dealer_price = "You must enter the dealer price";
  }
  return errs;
}

export default reduxForm({
  validate,
  form: 'InsertPianoForm'
})(
  connect(null, { createPiano })(InsertPiano)
);
