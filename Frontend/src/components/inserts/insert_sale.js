
import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createSale } from '../../actions/index';

class InsertSale extends Component {

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'had-danger' : ""}`;
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className='form-control'
          type='text'
          {...field.input}
        />
        <div className='text-help'>
          {touched ? error : ''}
        </div>
      </div>
    )
  }

  onSubmit(values) {

  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className='container'>
        <div className='jumbotron'>
          <form onSubmit={handleSubmit(this.onSubmit.bind(this))} >
            <Field
              label='Purchaser First Name'
              name='purchaser_first_name'
              component={this.renderField}
            />
            <Field
              label='Purchaser Last Name'
              name='purchaser_last_name'
              component={this.renderField}
            />
            <Field
              label='Purchaser Street'
              name='purchaser_street'
              component={this.renderField}
            />
            <Field
              label='Purchaser City'
              name='purchaser_city'
              component={this.renderField}
            />
            <Field
              label='Purchaser Zip'
              name='purchaser_zip'
              component={this.renderField}
            />
            <Field
              label='Purchaser Phone'
              name='purchaser_phone'
              component={this.renderField}
            />
            <Field
              label='Serial Number'
              name='serial_number'
              component={this.renderField}
            />
            <Field
              label='Salesman ID'
              name='salesman_id'
              component={this.renderField}
            />
            <Field
              label='Sale Date xxxx-xx-xx / 2017-11-05'
              name='saledate'
              component={this.renderField}
            />
            <Field
              label='Sale Price'
              name='sale_price'
              component={this.renderField}
            />
          </form>
      </div>
    </div>
    )
  }
}



function validate(values) {

  var errs = {};

  if(!values.purchaser_first_name) {
    errs.purchaser_first_name = "Please enter a valid first name";
  }
  if(!values.purchaser_last_name) {
    errs.purchaser_last_name = "Please enter a valid last name";
  }
  if(!values.purchaser_street) {
    errs.purchaser_street = "Please enter a valid street";
  }
  if(!values.purchaser_city) {
    errs.purchaser_city = "Please enter a valid city";
  }
  if(!values.purchaser_zip) {
    errs.purchaser_zip = "Please enter a valid zip";
  }
  if(!values.purchaser_phone) {
    errs.purchaser_phone = "Please enter a valid phone";
  }
  if(!values.serial_number) {
    errs.serial_number = "Please enter a valid serial number";
  }
  if(!values.salesman_id) {
    errs.salesman_id = "Please enter a valid salesman id";
  }
  if(!values.saledate) {
    errs.saledate = "Please enter a valid sale date";
  }
  if(!values.sale_price) {
    errs.sale_price = "Please enter a valid sale price";
  }

  return errs;
}

export default reduxForm({
  validate,
  form: 'InsertSaleForm'
})(
  connect(null, { createSale })(InsertSale)
)
