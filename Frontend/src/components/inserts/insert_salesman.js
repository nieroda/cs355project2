import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createSalesman } from '../../actions/index';

class InsertSalesman extends Component {

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'had-danger' : ""}`;
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className='form-control'
          type='text'
          {...field.input}
        />
        <div className='text-help'>
          {touched ? error : ''}
        </div>
      </div>
    )
  }

  onSubmit(values) {
    /**/
    this.props.createSalesman(values, () => {
      this.props.history.push('/');
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className='container'>
        <div className='jumbotron'>
          <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
            <Field
              label='Salesman ID'
              name='salesman_id'
              component={this.renderField}
              />
            <Field
              label='First Name'
              name='first_name'
              component={this.renderField}
              />
            <Field
              label='Last Name'
              name='last_name'
              component={this.renderField}
              />
            <Field
              label='SSN of Salesman'
              name='ssn'
              component={this.renderField}
              />
              <button type='submit' className='btn btn-primary'>Submit</button>
              <Link to='/' className='btn btn-danger'>Cancel</Link>
          </form>
        </div>
      </div>
    );
  }
}

function validate(values) {
  var errs = {};

  if (!values.salesman_id) {
    errs.salesman_id = 'You must give yourself an id';
  }
  if(!values.first_name) {
    errs.first_name = 'You must enter your first name...'
  }
  if (!values.last_name) {
    errs.last_name = 'You must enter your last name!'
  }
  if(!values.ssn) {
    errs.ssn = 'Please type in a fake SSN, we dont have SSL :(';
  }
  return errs;
}

export default reduxForm({
  validate,
  form: 'InsertSalesmanForm'
})(
  connect(null, { createSalesman })(InsertSalesman)
)
