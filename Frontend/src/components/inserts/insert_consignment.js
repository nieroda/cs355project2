import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createConsignment } from '../../actions/index';


class InsertConsignment extends Component {

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'had-danger' : ""}`;
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className='form-control'
          type='text'
          {...field.input}
        />
        <div className='text-help'>
          {touched ? error : ''}
        </div>
      </div>
    )
  }

  onSubmit(values) {
    this.props.createConsignment(values, () => {
      this.props.history.push('/');
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className='container'>
        <div className='text-center'>
          <Link to='/updateconsignment' className='btn btn-primary'>Update Consignment Sale</Link>
        </div>
        <br />
        <div className='jumbotron'>
          <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
            <Field
              label='Owner First Name'
              name='owner_first_name'
              component={this.renderField}
            />
            <Field
              label='Owner Last Name'
              name='owner_last_name'
              component={this.renderField}
            />
            <Field
              label='Owner Cell'
              name='owner_cell'
              component={this.renderField}
            />
            <Field
              label='Owner Email'
              name='owner_email'
              component={this.renderField}
            />
            <Field
              label='Owner Street'
              name='owner_street'
              component={this.renderField}
            />
            <Field
              label='Owner City'
              name='owner_city'
              component={this.renderField}
            />
            <Field
              label='Owner Zip'
              name='owner_zip'
              component={this.renderField}
            />
            <Field
              label='Date Started YYYY-MM-DD'
              name='datestarted'
              component={this.renderField}
            />
            <Field
              label='Date Sold / (null)'
              name='datesold'
              value='null'
              component={this.renderField}
            />
            <Field
              label='Salesman ID'
              name='salesman_id'
              component={this.renderField}
            />
            <Field
              label='Store Cut'
              name='store_cut'
              component={this.renderField}
            />
            <Field
              label='Serial Number'
              name='serial_number'
              component={this.renderField}
            />
            <button type='submit' className='btn btn-primary'>Submit</button>
            <Link to='/' className='btn btn-danger'>Cancel</Link>
          </form>
        </div>
      </div>
    )
  }
}

function validate(value) {
  var err = {};

  if (!value.owner_first_name) {
    err.owner_first_name = "Enter first name";
  }
  if (!value.owner_last_name) {
    err.owner_last_name = "Enter last name";
  }
  if (!value.owner_cell) {
    err.owner_cell = "Enter Cell";
  }
  if (!value.owner_email) {
    err.owner_email = "Enter Email";
  }
  if (!value.owner_street) {
    err.owner_street = "Enter Street";
  }
  if (!value.owner_city) {
    err.owner_city = "Enter city";
  }
  if (!value.owner_zip) {
    err.owner_zip = "Enter Zip";
  }
  if (!value.datestarted) {
    err.datestarted = "Enter date started";
  }
  if (!value.datesold) {
    err.datesold = "Enter date sold (null)";
  }
  if (!value.salesman_id) {
    err.salesman_id = "Enter your salesman id";
  }
  if (!value.store_cut) {
    err.store_cut = "Enter store cut";
  }
  if (!value.serial_number) {
    err.serial_number = "Enter serial number";
  }
  return err;
}

export default reduxForm({
  validate,
  form: 'InsertConsignmentForm'
})(
  connect(null, { createConsignment })(InsertConsignment)
)
