import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { createRental } from '../../actions/index';

class InsertRental extends Component {

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'had-danger' : ""}`;
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className='form-control'
          type='text'
          {...field.input}
        />
        <div className='text-help'>
          {touched ? error : ''}
        </div>
      </div>
    )
  }

  onSubmit(values) {
    this.props.createRental(values, () => {
      this.props.history.push('/');
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className='container'>
        <div className='text-center'>
          <Link to='/updaterental' className='btn btn-primary'>End Rental</Link>
        </div>
        <br />
        <div className='jumbotron'>
          <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
            <Field
              label='Renter First Name'
              name='renter_first_name'
              component={this.renderField}
            />
            <Field
              label='Renter Last Name'
              name='renter_last_name'
              component={this.renderField}
            />
            <Field
              label='Renter Street'
              name='renter_street'
              component={this.renderField}
            />
            <Field
              label='Renter City'
              name='renter_city'
              component={this.renderField}
            />
            <Field
              label='Renter Zip'
              name='renter_zip'
              component={this.renderField}
            />
            <Field
              label='Renter Phone'
              name='renter_phone'
              component={this.renderField}
            />
            <Field
              label='Renter SSN'
              name='renter_ssn'
              component={this.renderField}
            />
            <Field
              label='Rental Rate'
              name='rental_rate'
              component={this.renderField}
            />
            <Field
              label='Renters Relative First Name'
              name='renter_relative_first_name'
              component={this.renderField}
            />
            <Field
              label='Renters Relative Last Name'
              name='renter_relative_last_name'
              component={this.renderField}
            />
            <Field
              label='Date Started YYYY-MM-DD'
              name='date_started'
              component={this.renderField}
            />
            <Field
              label='Date Ended / (NULL)'
              name='date_ended'
              value='null'
              component={this.renderField}
            />
            <Field
              label='Serial Number'
              name='serial_number'
              component={this.renderField}
            />
            <Field
              label='Salesman ID'
              name='salesman_id'
              component={this.renderField}
            />
            <button type='submit' className='btn btn-primary'>Submit</button>
            <Link to='/' className='btn btn-danger'>Cancel</Link>
          </form>
        </div>
      </div>
    )
  }
}

function validate(values) {
  var errs = {};
  if (!values.renter_first_name) {
    errs.renter_first_name = "Enter your first name";
  }
  if (!values.renter_last_name) {
    errs.renter_last_name = "Enter your last name";
  }
  if (!values.renter_street) {
    errs.renter_street = "Enter your street please"
  }
  if (!values.renter_city) {
    errs.renter_city = "Enter your city";
  }
  if (!values.renter_zip) {
    errs.renter_zip = "Enter your zip";
  }
  if (!values.renter_phone) {
    errs.renter_phone = "Enter your phone!"
  }
  if (!values.renter_ssn) {
    errs.renter_ssn = "Enter your SSN (but don't actually)"
  }
  if (!values.rental_rate) {
    errs.rental_rate = "You must enter rental rate";
  }
  if (!values.renter_relative_first_name){
    errs.renter_relative_first_name = "You must enter your relatives first name";
  }
  if (!values.renter_relative_last_name) {
    errs.renter_relative_last_name = "You must enter your relatives last name";
  }
  if (!values.date_started) {
    errs.date_started = "You must enter a date in this form xxxx-xx-xx";
  }
  if (!values.date_ended) {
    errs.date_ended = "Enter 'null'";
  }
  if (!values.serial_number) {
    errs.serial_number = "You must enter a piano serial number";
  }
  if (!values.salesman_id) {
    errs.salesman_id = "You must enter your salesman id";
  } //whew
  return errs;
}

export default reduxForm({
  validate,
  form: 'InsertRentalForm'
})(
  connect(null, { createRental })(InsertRental)
)
