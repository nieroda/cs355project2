import React, { Component } from 'react';
import _ from 'lodash';

class CON_TABLE extends Component {

  mapData() {
    return _.map(this.props.data, item => {
      var { owner_first_name, owner_last_name, datestarted, datesold, store_cut } = item;
      return (
        <tr className='active' key={`${owner_first_name}${owner_last_name}`}>
          <td>{owner_first_name}</td>
          <td>{owner_last_name}</td>
          <td>{datestarted}</td>
          <td>{datesold}</td>
          <td>{store_cut}</td>
        </tr>
      );
    })
  }

  render() {
    return (
      <div className='table-responsive'>
        <h3> Consignments </h3>
        <table className='table table-bordered table-hover'>
          <tbody>
            <tr className='active'>
              <td>Owner First Name</td>
              <td>Owner Last Name</td>
              <td>Date Started</td>
              <td>Date Sold</td>
              <td>Store Cut</td>
              </tr>
              {this.mapData()}
          </tbody>
        </table>
      </div>
    );
  }
}

export default CON_TABLE;
