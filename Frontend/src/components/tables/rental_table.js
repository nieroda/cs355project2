import React, { Component } from 'react';
import _ from 'lodash';

class RentalTable extends Component {

  mapData() {
    return _.map(this.props.data, item => {
      var { renter_first_name, renter_last_name, rental_rate, date_started, date_ended, serial_number } = item;
      return (
        <tr className='active' key={`${renter_first_name}${renter_last_name}${date_started}`}>
          <td>{renter_first_name}</td>
          <td>{renter_last_name}</td>
          <td>{rental_rate}</td>
          <td>{date_started}</td>
          <td>{date_ended}</td>
          <td>{serial_number}</td>
        </tr>
      );
    })
  }

  render() {
    return (
      <div className='table-responsive'>
              <h3> Rentals </h3>
              <table className='table table-bordered table-hover'>
                <tbody>
                  <tr className='active'>
                      <td>Renter First Name</td>
                      <td>Renter Last Name</td>
                      <td>Rental Rate</td>
                      <td>Date Started</td>
                      <td>Date Ended</td>
                      <td>Serial Number</td>
                  </tr>
                  {this.mapData()}
                </tbody>
              </table>
            </div>
    );
  }
}

export default RentalTable;
