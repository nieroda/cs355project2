import React, { Component } from 'react';
import _ from 'lodash';

class SaleTable extends Component {

  mapData() {
    return _.map(this.props.data, item => {
      var { purchaser_first_name, purchaser_last_name, saledate, sale_price, dealer_price } = item;
      return (
        <tr className='active' key={`${purchaser_first_name}${purchaser_last_name}`}>
          <td>{purchaser_first_name}</td>
          <td>{purchaser_last_name}</td>
          <td>{saledate}</td>
          <td>{sale_price}</td>
          <td>{dealer_price}</td>
        </tr>
      );
    });
  }


  render() {
    return (
      <div className='table-responsive'>
        <h3> Sales </h3>
        <table className='table table-bordered table-hover'>
          <tbody>
            <tr className='active'>
                <td>Purchaser First Name</td>
                <td>Purchaser Last Name</td>
                <td>Sale Date</td>
                <td>Sale Price</td>
                <td>Dealer Price</td>
            </tr>
            {this.mapData()}
          </tbody>
        </table>
    </div>
    );
  }
};

export default SaleTable;
