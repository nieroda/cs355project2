import React, { Component } from 'react';
import _ from 'lodash';

class STble extends Component {

  mapData() {
    return _.map(this.props.data, item => {
      var { brand, model, dealer_price, sale_price, first_name } = item;
      return (
          <tr className='active' key={`${brand}${sale_price}${model}${first_name}`}>
            <td>{brand}</td>
            <td>{model}</td>
            <td>{dealer_price}</td>
            <td>{sale_price}</td>
            <td>{sale_price - dealer_price}</td>
            <td>{first_name}</td>
          </tr>
      );
    });
  } 

  render() {
    return (
      <div className='table-responsive'>
        <h3> Pianos Sold </h3>
        <table className='table table-bordered table-hover'>
          <tbody>
            <tr className='active'>
                <td>Brand</td>
                <td>Model</td>
                <td>Dealer Price</td>
                <td>Sale Price</td>
                <td>Profit</td>
                <td>Salesman</td>
            </tr>
            {this.mapData()}
          </tbody>
        </table>
      </div>
    );
  }
};

export default STble;
