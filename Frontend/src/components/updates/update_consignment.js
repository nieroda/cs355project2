import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { updateConsignment } from '../../actions/index';

class UpdateConsignment extends Component {

  renderField(field) {
    const { meta: { touched, error } } = field;
    const className = `form-group ${touched && error ? 'had-danger' : ""}`;
    return (
      <div className={className}>
        <label>{field.label}</label>
        <input
          className='form-control'
          type='text'
          {...field.input}
        />
        <div className='text-help'>
          {touched ? error : ''}
        </div>
      </div>
    )
  }

  onSubmit(values) {
    this.props.updateConsignment(values, () => {
      this.props.history.push('/');
    });
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div className='container'>
        <div className='jumbotron'>
          <form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
            <Field
              label='Salesman ID'
              name='salesman_id'
              component={this.renderField}
              />
              <Field
                label='Serial Number'
                name='serial_number'
                component={this.renderField}
                />

              <button type='submit' className='btn btn-primary'>Submit</button>
              <Link to='/' className='btn btn-danger'>Cancel</Link>
          </form>
        </div>
      </div>
    )
  }
}

function validate(values) {
  var errs = {};

  if (!values.salesman_id) {
    errs.salesman_id = "you must enter a valid salesman id";
  }
  if (!values.serial_number) {
    errs.serial_number = "you must enter a serial number";
  }
  return errs;
}

export default reduxForm({
  validate,
  form: 'UpdateConsignmentForm'
})(
  connect(null, { updateConsignment })(UpdateConsignment)
)
