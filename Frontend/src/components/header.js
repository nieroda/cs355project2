import React, { Component } from 'react';
import { Link } from 'react-router-dom';

//dumb component


class MyHeaders extends Component {
    render() {
        return (
            <div>
            <nav className='navbar navbar-default'>
                <div className='container-fluid'>
                    <div className='navbar-header'>
                        <Link className='navbar-brand' to='/'>
                            Nathans Pianos
                        </Link>
                    </div>
                    <ul className='nav navbar-nav'>
                        <li><Link to='/salesresult'>Sale Details</Link></li>
                        <li><Link to='/insertpiano'>Add Piano</Link></li>
                        <li><Link to='/insertsalesman'>Add Salesman</Link></li>
                        <li><Link to='/insertsale'>Add Sale</Link></li>
                        <li><Link to='/insertrental'>Add Rental</Link></li>
                        <li><Link to='/insertconsignment'>Add Consignment</Link></li>
                    </ul>
                </div>
            </nav>
            </div>
        );
    }
}

export default MyHeaders;
