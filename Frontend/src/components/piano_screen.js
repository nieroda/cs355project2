import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class PianoScreen extends Component {

  render() {
    return (
      <div className='container' id='only'>
        <div className='row'>
          <div id='content'>
            <h1 id='specialH1'> Piano Database </h1>
            <h3> For Management Only </h3>
            <hr id='speicalhr'/>
            <button className='btn btn-default btn-lg'>
              <Link to='/salesresult'>GO! </Link>
            </button>
          </div>
        </div>
        <br/><br/><br/><br/><br/><br/>
      </div>
    );
  }
}
export default PianoScreen;
