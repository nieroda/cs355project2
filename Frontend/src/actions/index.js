import axios from 'axios';

export const FETCH = 'GETSALESRESULT';
export const CREATE_PIANO = 'CREATEPIANO';
export const CREATE_SALESMAN = 'CREATESALESMAN';
export const CREATE_RENTAL = 'CREATERENTAL';
export const CREATE_CONSIGNMENT = 'CREATECONSIGNMENT';
export const CREATE_SALE = 'CREATESALE';
export const UPDATE_RENTAL = 'UPDATERENTAL';
export const UPDATE_CONSIGNMENT = 'UPDATECONSIGNMENT';

const ROOT_URL = 'http://localhost:3000';
console.log('called');
export function fetchPosts() {
  const request = axios.get(`${ROOT_URL}/salesresult`)
  return {
    type: FETCH,
    payload: request
  };
}

export function createPiano(values, callback) {
  const request = axios.post(`${ROOT_URL}/insertpiano`, values)
  .then(() => callback());
  return {
    type: CREATE_PIANO,
    payload: request
  }
}

export function createSalesman(values, callback) {
  const request = axios.post(`${ROOT_URL}/insertsalesman`, values)
  .then(() => callback());

  return {
    type: CREATE_SALESMAN,
    payload: request
  }
}

export function createRental(values, callback) {
  const request = axios.post(`${ROOT_URL}/insertrental`, values)
  .then(() => callback());

  return {
    type: CREATE_RENTAL,
    payload: request
  }
}

export function createConsignment(values, callback) {
  const request = axios.post(`${ROOT_URL}/insertconsignment`, values)
  .then(() => callback());

  return {
    type: CREATE_CONSIGNMENT,
    payload: request
  }
}

export function createSale(values, callback) {
  const request = axios.post(`${ROOT_URL}/insertsale`, values)
  .then(() => callback());

  return {
    type: CREATE_SALE,
    payload: request
  }
}

export function updateRental(values, callback) {
  const request = axios.put(`${ROOT_URL}/updaterental`, values)
  .then(() => callback());

  return {
    type: UPDATE_RENTAL,
    payload: request
  }
}

export function updateConsignment(values, callback) {
  const request = axios.put(`${ROOT_URL}/updateconsignment`, values)
  .then(() => callback());

  return {
    type: UPDATE_CONSIGNMENT,
    payload: request
  }
}
